all: compile
	
compile:
	mpicc -o 4 4.thread.c -lpthread -lm
	mpicc -o 6 6.MPI.c
	mpicc -o 7 7.MPI+thread.c -lpthread -lm
	gcc -o ascii tabela_ascii.c

clean:
	rm ex?
	rm -rf *.out

