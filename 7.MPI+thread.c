/**
* Trabalho II - quebra de senha com MPI
* Desenvolver um programa que execute em paralelo utilizando gcc/MPI 
* para quebrar uma senha de 5(cinco) chars que execute com n processos.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include "mpi.h"

#define QT 8

char linha[40], senha[40], senha2[40];
int numprocs;
int fim_p;
int inicio_p;


int calculaInicioEscravo(qtd_escravos, intervalo, myid);
int calculaFimEscravo(qtd_escravos, intervalo, myid, inicio_p);
void criaThreads(pthread_t threads[QT]);
void *crash(void *arg);

int main (int argc, char *argv[]) {
    double startwtime = 0.0,
    		endwtime   = 0.0;

    pthread_t threads[QT];

    int myid, tam;
    MPI_Status status;
    MPI_Init(&argc,&argv); // inicia o mpi
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs); //Determina o tamanho do grupo associado a um comunicador
    MPI_Comm_rank(MPI_COMM_WORLD,&myid); //Determina a classificação do processo de chamada no comunicador


    if (myid == 0){
    /*** MESTRE ***/

        //startwtime = MPI_Wtime();
        
        FILE *fptr;

        /* Tenta abrir o arquivo contendo a senha */
        if ((fptr = fopen ("password.txt","rb")) == NULL)
        { 
            printf ("\nNão posso abrir o arquivo!\n");
            exit(1);
        }

        /* Guarda a senha em uma variável */
        fgets(linha,40,fptr);
        fclose(fptr);

        /* Pega o tamanho real da senha */
        tam = strlen(linha);
        
        /* Mestre envia mensgem para todos os escravos */
        for (int i = 1; i < numprocs; i++)
        {
            MPI_Send(linha, tam, MPI_UNSIGNED_CHAR, i, 8, MPI_COMM_WORLD);
        }

        printf("\n");

        /* Mestre recebe mensagem de qualquer escravo */
        for (int i = 1; i < numprocs; i++)
        {
            MPI_Recv(senha2, tam, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, 88, MPI_COMM_WORLD, &status);
        }

        printf("\n");
        //endwtime = MPI_Wtime();
        //printf("\nTempo de execucao: %f segundos\n\n", endwtime-startwtime);

    }else{
    /*** ESCRAVO ***/

        /* Escravo recebe mensagem do mestre */
        MPI_Recv(linha, 5, MPI_UNSIGNED_CHAR, 0, 8, MPI_COMM_WORLD, &status);

        int qtd_escravos = numprocs-1;
        int intervalo = (int)95/qtd_escravos;

        inicio_p = calculaInicioEscravo(qtd_escravos, intervalo, myid);
        fim_p = calculaFimEscravo(qtd_escravos, intervalo, myid, inicio_p);

        criaThreads(threads);

        printf("\n");
    }

    MPI_Finalize();
    return 0;
}


/*Função que calcula o inicio do intervalo de trabalho do escravo*/
int calculaInicioEscravo(int qtd_escravos, int intervalo, int myid) {
    
    intervalo = (int)95/qtd_escravos;

    if(myid == 1){
        inicio_p = 32;
    }else{
        inicio_p = intervalo*(myid-1) + 32;
    }

    return inicio_p;
}


/*Função que calcula o fim do intervalo de trabalho do escravo*/
int calculaFimEscravo(int qtd_escravos, int intervalo, int myid, int inicio_p) {

    /* Detecta último processo e força ele a ir até o fim da tabela */
    if(myid == qtd_escravos){
        fim_p = 127;
    }else{
        fim_p = inicio_p + (intervalo-1);
    }

    return fim_p;
}



/*Função de criação das threads*/
void criaThreads(pthread_t threads[QT]) {

    for(int t = 0; t < QT; t++)
    {   
        int *threadNum = malloc(sizeof(*threadNum));
        if ( threadNum == NULL ) {
            fprintf(stderr, "Couldn't allocate memory for thread arg.\n");
            exit(EXIT_FAILURE);
        }

        *threadNum = t;

        pthread_create(&threads[t], NULL, *crash, threadNum);
    }
    
    for(int j = 0; j < QT; j++)
    {
        pthread_join(threads[j], NULL);
    }
}

/*Função de quebra de senha*/
void *crash(void *arg) {

	double startwtime = 0.0,
    		endwtime   = 0.0;

	startwtime = MPI_Wtime();

    int ok, cont, inicio_t, fim_t;

    int t = *((int*)arg); //pega o argumento da thread que é o seu índice

    senha[strlen(linha)]='\0';

    /*Calcula o início da thread*/
    if(t == 0){
        inicio_t = inicio_p;
    }else{
        inicio_t = inicio_p + (fim_p - inicio_p)/QT;
    }

    /*Calcula o fim da thread*/
    if(t == (QT-1)){
        fim_t = fim_p;
    }else{
        fim_t = inicio_t + ((fim_p - inicio_p)/QT)-1;
    }


    /****************************
    * Início da quebra de senha *
    *****************************/
    for(int b = inicio_t; b < fim_t; b++)
    {
        senha[0] = b; 

        for(int c = 32; c < 127; c++) 
        {
            senha[1] = c;

            for(int d = 32; d < 127; d++)
            {
             senha[2] = d;

                for(int e = 32; e < 127; e++) 
                {
                    senha[3] = e;

                    for(int f = 32; f < 127; f++) 
                    {
                        senha[4] = f;
                        cont ++;
                        ok = strcmp(senha, linha);
                        //printf("\nESCRAVO: %d INÍCIO: %d FIM: %d", myid, inicio, fim);
                        if(ok == 0)
                        {
                            printf("\n");
                            printf("\nSENHA ENCONTRADA: %s\n", senha);
                            endwtime = MPI_Wtime();
        					printf("\nTempo de execucao: %f segundos\n\n", endwtime-startwtime);
                            MPI_Send(senha, 5, MPI_UNSIGNED_CHAR, 0, 88, MPI_COMM_WORLD);
                            pthread_exit(0);
                        }
                    }
                }
            }
        }
    }
    MPI_Send(senha, 5, MPI_UNSIGNED_CHAR, 0, 88, MPI_COMM_WORLD);
    pthread_exit(NULL);
}