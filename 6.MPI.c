/**
* Trabalho II - quebra de senha com MPI
* Desenvolver um programa que execute em paralelo utilizando gcc/MPI 
* para quebrar uma senha de 5(cinco) chars que execute com n processos.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "tempo.h"
#include <string.h>
#include <math.h>
#include "mpi.h"

char linha[40], senha[40], senha2[40];
int numprocs;

void crash (int tam, int myid);

int main (int argc, char *argv[])
{
    double startwtime = 0.0,
    endwtime   = 0.0;

    int myid, tam;
    MPI_Status status;
    MPI_Init(&argc,&argv); // inicia o mpi
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs); //Determina o tamanho do grupo associado a um comunicador
    MPI_Comm_rank(MPI_COMM_WORLD,&myid); //Determina a classificação do processo de chamada no comunicador


    if (myid == 0){
    /*** MESTRE ***/

        startwtime = MPI_Wtime();
        
        FILE *fptr;

        /* Tenta abrir o arquivo contendo a senha */
        if ((fptr = fopen ("password.txt","rb")) == NULL)
        { 
            printf ("\nNão posso abrir o arquivo!\n");
            exit(1);
        }

        /* Guarda a senha em uma variável */
        fgets(linha,40,fptr);
        fclose(fptr);

        /* Pega o tamanho real da senha */
        tam = strlen(linha);
        
        /* Mestre envia mensgem para todos os escravos */
        for (int i = 1; i < numprocs; i++)
        {
            MPI_Send(linha, tam, MPI_UNSIGNED_CHAR, i, 8, MPI_COMM_WORLD);
        }

        printf("\n");

        /* Mestre recebe mensagem de qualquer escravo */
        for (int i = 1; i < numprocs; i++)
        {
            MPI_Recv(senha2, tam, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, 88, MPI_COMM_WORLD, &status);
        }

        printf("\n");
        endwtime = MPI_Wtime();
        printf("\nTempo de execucao: %f segundos\n\n", endwtime-startwtime);

    }else{
    /*** ESCRAVO ***/

        /* Escravo recebe mensagem do mestre */
        MPI_Recv(linha, 5, MPI_UNSIGNED_CHAR, 0, 8, MPI_COMM_WORLD, &status);
        crash(tam, myid);
        printf("\n");
    }

    MPI_Finalize();
    return 0;
}


void crash (int tam, int myid)
{
    int inicio, qtd_escravos, intervalo, fim, ok, cont;
    senha[strlen(linha)]='\0';

    qtd_escravos = numprocs-1;
    intervalo = (int)95/qtd_escravos;


    
    if(myid == 1){
        inicio = 32;
    }else{
        inicio = intervalo*(myid-1) + 32;
    }


    /* Detecta último processo e força ele a ir até o fim da tabela */
    if (myid == qtd_escravos){
        fim = 127;
    }else{
        fim = inicio + (intervalo-1);
    }


    //printf("\n");
    //printf("ESCRAVO: %d INICIO: %d FIM: %d", myid, inicio, fim);

    /****************************
    * Início da quebra de senha *
    *****************************/
    for (int b = inicio; b < fim; b++)
    {
        senha[0] = b; 

        for (int c = 32; c < 127; c++) 
        {
            senha[1] = c;

            for (int d = 32; d < 127; d++)
            {
             senha[2] = d;

                for (int e = 32; e < 127; e++) 
                {
                    senha[3] = e;

                    for (int f = 32; f < 127; f++) 
                    {
                        senha[4] = f;
                        cont ++;
                        ok = strcmp(senha, linha);
                        //printf("\nESCRAVO: %d INÍCIO: %d FIM: %d", myid, inicio, fim);
                        if (ok == 0)
                        {
                            printf("\n");
                            printf("\nSENHA ENCONTRADA: %s\n", senha);
                            MPI_Send(senha, tam, MPI_UNSIGNED_CHAR, 0, 88, MPI_COMM_WORLD);
                        }
                    }
                }
            }
        }
    }
    MPI_Send(senha, tam, MPI_UNSIGNED_CHAR, 0, 88, MPI_COMM_WORLD);
}